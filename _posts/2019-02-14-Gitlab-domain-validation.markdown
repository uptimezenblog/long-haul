---
layout: post
title:  "Tutorial: Gitlab Domain Verification using Godaddy"
date:   2019-02-14
description: Gitlab Domain Validation with GoDaddy
---

<p class="intro"><span class="dropcap">G</span>itlab domain verification system was introduced for Gitlab Pages custom domains to preventing domains from being hijacked by unauthorized users. This process looks pretty easy but sometimes it gets tricky during validation process.</p> 

In this arcticle , goal is to put you at ease and show tricky steps when you are attempting to add CNAME and TXT records for subdomain from Godaddy account for your Gitlab projects. When you complete the steps below , your subdomain (blog.example.com) will be redirected to Gitlab pages and your projects will be accessible over blog.example.com 

Before starting configuration steps , it is assumed that ; 

 * You have already domain (Godaddy or another hosting company) 
 * You have already create website on Gitlab
 * Your website is running without any issue and accessible from https://username.gitlab.io or https://groupname.gitlab.io (if not please make your project public from project settings) 


**Adding Domain on Gitlab**

**Step 1:** Login to Gitlab then select project that you want to add custom domain 

**Step 2:** Navigate to Pages section from Settings --> Pages (you can find the links of your personal site like https://username.gitlab.io or company site https://groupname.gitlab.io)

**Step 3:** Uncheck the "Force domain with SSL certificate to use HTTPS" then save. (If you have already SSL certificate you do not need to remove checks)

**Step 4:** Click on "New Domain" button to add your domain. 

**Step 5:** Enter the domain name. (you can add domain or subdomain, in this case our goal is to add subdomain for blog page )

e.g : blog.uptimezen.com 

**Step 6:** Click on "Save Changes" button then your newly added domain will be visible on "Pages" 

**Step 7:** You will be presented with error message "example.com is not verified. To learn how to verify ownership, visit your domain details".  This message appears on the screen because you do not verify domain from Godaddy. (Godady given as sample , if you are using another hosting provider you can manage from DNS settings section as well)

**Step 8:** Click on "Details" button then you will see the verification code under "Verification Status" section.

We will create new CNAME and TXT record on Godaddy with using these "verification status" and "dns" record displayed on "Pages Domain"

**Creating CNAME record on Godady for Gitlab custom domains**

Before starting configuration steps please login to Godaddy account then navigate to DNS Management settings of the domain. You can find detailed information from https://www.godaddy.com/help/add-a-cname-record-19236?

**Step 9:** Click on "Add" button to add new record 

**Step 10:** Select type as "CNAME" 

**Step 11:** You have to enter CNAME values that is published on "Pages Domain" section (Step 8). 

CNAME example: 

host : please enter the subdomain that you want to redirect , exm : blog.example.com 
Points to : please enter gitlab domain assigned to your project , exm : example.gitlab.io 

then click on "Save button"


**Creating TXT record on Godady for Githab custom domains**

In this step , if you are attempting to add a TXT record for subdomain to allow Gitlab to verify ownership but you receive the error the following error "Failed to verify domain ownership" please

follow the steps below. We need to make changes on the TXT values provided by Gitlab (Step 8)  

In my case deafult value is; 
```
_gitlab-pages-verification-code.blog.uptimezen.com TXT gitlab-pages-verification-code=e27e34333d4f0f68cc 
```

Please remove the domain and keep only blog 
```
_gitlab-pages-verification-code.blog TXT gitlab-pages-verification-code=e27e34333d4f0f68cc
```
then use modified values as TXT records. 

**Step 11:** Click Add button to add new TXT records 

**Step 12:** Select type as TXT 

**Step 13:** Enter HOST value , this should be modified before adding it 

In my case deafult host value is; 
```
_gitlab-pages-verification-code.blog.uptimezen.com TXT gitlab-pages-verification-code=e27e34333d4f0f68cc 
```
Please remove the domain from host value and keep only subdomain "blog"
```
_gitlab-pages-verification-code.blog TXT gitlab-pages-verification-code=e27e34333d4f0f68cc
```
then use modified values as TXT records. 

**Step 14:** Enter the TXT Value without making change on it then save it. 


**Verification**

**Step 15:** To verify domain ownership connect to gitlab account then navigate to Settings --> Pages. 

**Step 16:** Then click on "Details" button to see "Verification Status". You can see "Verified" then everyting is Ok and your subdomain is ready to use with your gitlab project. 

If status seen as "Unverified" please click on refresh. 















